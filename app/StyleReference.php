<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StyleReference extends Model
{
    protected $table = 'style_reference';
    protected $fillable = array('id','email','ques1','ques2','ques3');
}

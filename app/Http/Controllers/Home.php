<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\SendMail;
use App\Mail\SendContactusMail;
use App\Mail\SendPartnerMail;
use App\Mail\SendStyleRefMail;
use App\Mail\SendStyleRefAdminMail;
use App\Newsletter;
use App\Banners;
use App\Lookbook;
use App\Products;
use App\Contact;
use App\Partners;
use App\StyleReference;
use Illuminate\Support\Facades\DB;

class Home extends Controller
{
    //newsletter - send email
    public function newsletter(Request $request){
        $errors = array();
        $data = array();
    
    if(empty($request->input('newsletteremail')))
        $errors['newsletteremail'] = 'Email is required';
    
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $email = $request->input('newsletteremail');
            
//            $data['success'] = true;
//            $data['successMessage'] = 'Thank you.We will get back to you.';
            
            //Mail::to($email)->send(new SendMail) or die('Got Error');
            
//            Mail::send('email.newsletter',function($message){
//               $message->to($email)->subject('Test Mail'); 
//            });
            
            //\Mail::to($email)->send(new SendMail);
            
            /*
            if(\Mail::to($email)->send(new SendMail)){
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }else{
                $errors['errorMessage'] = 'Error';
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            */
            
            $newsletter = new Newsletter;
            $newsletter->email = $email;
            $newsletter->is_active = 1;            
            
            try {
                \Mail::to($email)->send(new SendMail($email));
                
                $newsletter->save();
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
            
        }  
        
        return json_encode($data);
    }
    
    public function getbanners(){
        $p = $_REQUEST['p'];
        $m = $_REQUEST['m'];
        $data = array();
        
        $banners = DB::table('banners')
                    ->where('page',$p)
                    ->where('is_active',1)
                    ->where('is_Mobile',$m)
                    ->orderBy('sequence','asc')
                    ->get();
                
        return json_encode($banners);
    }
    
    public function getlookbook($id = null){
        if($id == null){
            //return Lookbook::orderBy('id','desc')->get();
            $lookbook = DB::table('lookbook')
                    ->select('id','title','lb_short_description','lb_long_description','category','banner_img','similar_1','similar_2','similar_3','similar_4','look_1','look_2','look_3','created_at')
                    ->orderBy('id','desc')
                    ->get();
            return json_encode($lookbook);
        }else{
            return $this->show($id);
        }
    }
    
    public function getfeaturedproducts(){
        
        $featuredproducts = DB::table('products')
                    ->where('isFeatured',1)
                    ->groupBy('VC_SKU')
                    ->limit(12)
                    ->get();
                
        if($featuredproducts != null){
            return json_encode($featuredproducts);
        }
        else{
            return "invalid";
        }     
    }
    
    public function saveContactusform(Request $request)
    {
        $errors = array();
        $data = array();
    
    if(empty($request->input('cname')))
        $errors['name'] = 'Name is required';

    if(empty($request->input('cemail')))
        $errors['email'] = 'Email is required';

    if(empty($request->input('cphone')))
        $errors['phone'] = 'Phone is required';
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $contact = new Contact;
        
            $contact->name = $request->input('cname');
            $contact->email = $request->input('cemail');
            $contact->mobile = $request->input('cphone');
            $contact->message = $request->input('cmessage');
            
            $data = array(
                'name' => $request->input('cname'),
                'email' => $request->input('cemail'),
                'phone' => $request->input('cphone'),
                'messagedata' => $request->input('cmessage')
            );
            
            try {
                $contact->save();
                
                $emailto = 'sadhana@vivocarat.com';
                \Mail::to($emailto)->send(new SendContactusMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    public function savePartnerform(Request $request)
    {
        $errors = array();
        $data = array();
    
    if(empty($request->input('pname')))
        $errors['name'] = 'Name is required';

    if(empty($request->input('pemail')))
        $errors['email'] = 'Email is required';

    if(empty($request->input('pphone')))
        $errors['phone'] = 'Phone is required';
        
        if ( ! empty($errors)) {           
		  $data['success'] = false;
		  $data['errors']  = $errors;	
        } else {
            
            $partner = new Partners;
        
            $partner->name = $request->input('pname');
            $partner->email = $request->input('pemail');
            $partner->phone = $request->input('pphone');
            $partner->brand = $request->input('pbrand');
            $partner->address = $request->input('paddress');
            
            $data = array(
                'name' => $request->input('pname'),
                'email' => $request->input('pemail'),
                'phone' => $request->input('pphone'),
                'brand' => $request->input('pbrand'),
                'address' => $request->input('paddress')
            );
            
            try {
                $partner->save();
                
                $emailto = 'sadhana@vivocarat.com';
                \Mail::to($emailto)->send(new SendPartnerMail($data));
                
                $data['success'] = true;
                $data['successMessage'] = 'Thank you.We will get back to you.';
            }
            catch (\Exception $e) {
                $errors['errorMessage'] = $e->getMessage();
                $data['success'] = false;
                $data['errors'] = $errors;
            }
            
        }  
        
        return json_encode($data);
    }
    
    public function getLookbookDetail(){
        
        $id = $_REQUEST['id'];
        //$sql2="SELECT * from lookbook where id=".$_GET['id'];
        $detail = DB::table('lookbook')
                    ->where('id',$id)
                    ->get();
        
        return json_encode($detail);
    }
    
    public function getLookbookList(){
        //$sql2="SELECT * from lookbook order by id DESC";
        $lookbooklist = DB::table('lookbook')
                    ->orderBy('id','desc')
                    ->get();
        return json_encode($lookbooklist);
    }
    
    public function getRelatedBlog(){
        $id1= $_REQUEST['id1'];
        $id2 = $_REQUEST['id2'];
        $id3 = $_REQUEST['id3'];

        //$sql2="SELECT * from lookbook  where id=".$id1." or id=".$id2." or id=".$id3 ;
        
//        $relatedblog = DB::table('lookbook')
//                    ->where('id',$id1)
//                    ->orWhere('id',$id2)
//                    ->orWhere('id',$id3)
//                    ->get();
        
//        $relatedblog = DB::table('lookbook')
//                    ->whereIn('id', [$id1, $id2, $id3])
//                    ->get();
        
        $relatedblog = DB::table('lookbook')
                    ->where('id',$id1)
                    ->orWhere('id',$id2)
                    ->orWhere('id',$id3)
                    ->get();
        
        return json_encode($relatedblog);
    }
    
    public function getSimilarProducts(){
        
        $s1= $_REQUEST['s1'];
        $s2 = $_REQUEST['s2'];
        $s3 = $_REQUEST['s3'];
        $s4 = $_REQUEST['s4'];

        //$sql2="SELECT * from products  where id=".$s1." or id=".$s2." or id=".$s3." or id=".$s4 ;
        
//        $similarproducts = DB::table('products')
//                    ->where('id',$s1)
//                    ->orWhere('id',$s2)
//                    ->orWhere('id',$s3)
//                    ->orWhere('id',$s4)
//                    ->get();
        
        $similarproducts = DB::table('products')
                    ->whereIn('id', [$s1, $s2, $s3, $s4])
                    ->get();
        
        return json_encode($similarproducts);
    }
    
    public function saveStylereference(){
        $msg= array();
        $email = $_REQUEST['email'];
        $ques1 = $_REQUEST['collectionid'];
        $ques2 = $_REQUEST['skinid'];
        $ques3 = $_REQUEST['styleid'];
        
        $style = new StyleReference;
        
        $style->email= $email;
        $style->ques1 = $ques1;
        $style->ques2 = $ques2;
        $style->ques3 = $ques3;
        
        try 
        {
            $style->save();
            $data = array(
                    'email' => $email
                );

            \Mail::to($email)->send(new SendStyleRefMail($data));

            $adminemail = "sadhana@vivocarat.com";

            \Mail::to($adminemail)->send(new SendStyleRefAdminMail($data));
            
            $msg['success'] = true;
            $msg['successMessage'] = 'Thank you.';
        }
        catch (\Exception $e) {
            $errors['errorMessage'] = $e->getMessage();
            $msg['success'] = false;
            $msg['errors'] = $errors;
        }
        return json_encode($msg);        
    }
}
<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PriceDropAlert extends Model
{
    protected $table = 'price_drop_alert';
    protected $fillable = array('id','email','phone','pid');
}

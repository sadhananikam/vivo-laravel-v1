<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class SendStyleRefAdminMail extends Mailable
{
    use Queueable, SerializesModels;
    protected $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($request)
    {
        $this->data = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $address = 'sadhana@vivocarat.com';
        $name = 'Vivocarat Support';
        $subject = 'New Style Reference to Vivocarat';
        
        return $this->view('email.stylerefadmin')
                    ->with([
                        'email'=>$this->data['email']
                           ])
                    ->from($address,$name)
                    ->replyTo($address,$name)
                    ->subject($subject);
    }
}

<html>
 <head>
  <style>

   @font-face {
            font-family: 'Leelawadee';
            src: url('https://www.vivocarat.com/fonts/Leelawadee.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }

        body {
            font-family: 'Leelawadee',sans-serif;
            font-size: 15px;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }
 </style>

     <link href='https://www.vivocarat.com/fonts/Leelawadee.ttf' rel='stylesheet' type='text/css'>

</head>

<body align='center' style="font-family: 'Leelawadee';font-size: 15px;max-width:600px;margin:0 auto;">

 <table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
   
  <tr>
  <td>
   <table cellpadding='0' cellspacing='0' align='center' style='padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;'> 
    <tr>
     <td align='center'>
      <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='Vivocarat logo' title='Vivocarat logo'>  
     </td>
    </tr>
   </table>
  </td>
 </tr>

<tr>
  <td align='center'>
   <a href='https://play.google.com/store/apps/details?id=com.vivocarat.catalogapp&hl=en' style='text-decoration:none;'>
    <img src='https://www.vivocarat.com/images/emailers/vivocarat-app.jpg' alt='Vivocarat Android app' title='Vivocarat Android app' style="width:100%;display:block;">   
   </a>
  </td>  
 </tr>

<tr style='text-align:left;width:100%;border: 1px solid #e3e3e3;border-bottom: none;'>
 <table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
  <tr>
   <td>
    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Hello <b> {{ $name1 }},</b>
    </p>

    <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Greetings from VivoCarat.com!
    </p>

    <p style='margin-bottom:20px;margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Thank you for choosing Vivocarat.com and purchasing from us.
    </p>

    <p style='margin-left:20px;margin-right:20px;margin-bottom:15px;font-family: Leelawadee;'>
        Your order details are as below
    </p>

@php
$temp="";
$items=json_decode($order1);
$temp="

<table style='font-size: 13px;border-collapse: collapse;'>
 <tr style='text-align: center;height: 30px;background-color: #c7c7c7;'>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>SKU</th>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>Name</th>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>Jeweller</th>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>Size</th>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>Quantity</th>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>Price before discount</th>
  <th style='border-right: 1px solid #e3e3e3;font-family: Leelawadee;'>Discount</th>
  <th style='font-family: Leelawadee;'>Price after discount</th>
 </tr>


";

foreach($items as $i){


$temp=$temp.'<tr>
<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;padding: 10px 1px;font-family: Leelawadee;">'.$i->item->VC_SKU."</td>";

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;font-family: Leelawadee;">'.$i->item->title."</td>";

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;font-family: Leelawadee;">'.$i->item->supplier_name."</td>";

if($i->item->ring_size != null && $i->item->ring_size != '')
{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;'>".$i->item->ring_size."</td>";
}
elseif($i->item->bangle_size != null && $i->item->bangle_size != '')
{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;'>".$i->item->bangle_size."</td>";
}
elseif($i->item->bracelet_size != null && $i->item->bracelet_size != '')
{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;'>".$i->item->bracelet_size."</td>";
}
else{
    $temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;'>NA</td>"; 
}

$temp=$temp."<td style='border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;'>".$i->quantity."</td>";

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;">'.$i->item->price_before_discount * $i->quantity."/-</td>";

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;">'.$i->item->discount."%</td>";

$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;text-align: right;font-family: Leelawadee;">'.$i->item->price_after_discount * $i->quantity."/-</td>";

}
$temp=$temp.'</tr>';


$temp=$temp.'<tr><td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;height: 25px;padding: 10px 1px;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;color: #E62739;width:103px;font-family: Leelawadee;">Promotional Offer</td>';

$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;color: #E62739;text-align: right;padding-right: 5px;font-family: Leelawadee;">'.$promo1.'</td>';


$temp=$temp.'</tr>';


$temp=$temp.'<tr><td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;height:25px;padding: 10px 1px;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;"></td>';

$temp=$temp.'<td style="border-right: 1px solid #e3e3e3;border-bottom: 1px solid #e3e3e3;color:#E62739;text-align: right;padding-right: 5px;font-family: Leelawadee;">Total</td>';

$temp=$temp.'<td style="border-bottom: 1px solid #e3e3e3;color:#E62739;text-align: right;padding-right: 5px;font-family: Leelawadee;">'.$total1.'/-</td>';


$temp=$temp.'</tr></table>';

$temp=$temp.'<h3 style="color: #E62739;font-weight:bold;margin-left:20px;margin-right:20px;font-family: Leelawadee;">Total : '.$total1.'/-</h3>';

echo $temp;
@endphp

<p style='margin-bottom:20px;margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
   Your Order No. {{ $id1 }} has been confirmed. We will send your delivery details shortly.
</p>

<p style='margin-bottom:20px;margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
   We hope you had a wonderful experience and look forward for a positive feedback from your end. Do visit us again!
</p>

<p style='margin-bottom:20px;text-decoration:none;color:#000000 !important;margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
   In case of any query or assistance reach us at +91 9503 781 870 or mail us to 
   <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color: #000000!important;font-family: Leelawadee;'>
       hello@vivocarat.com.
   </a>
</p>

<p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
   Warm Regards,
</p>
       
<p style='margin-bottom:0px;margin-left:20px;margin-right:20px;padding-bottom: 20px;font-family: Leelawadee;'>
 <b>Team VivoCarat.</b>
</p>
       
   </td>
  </tr>   
 </table>
</tr>

<table align='center' style='height: 50px;width:100%;background-color:#E62739;border-right: solid 1px #d4d4d4;border-left: solid 1px #d4d4d4;border-top: solid 1px #d4d4d4;'>
 <tr>
  <td style='font-size:13px;color:white;text-align: left;padding:7px;color:white;font-family: Leelawadee;'>+91 9167 645 314</td>
     
  <td style='font-size:13px;color:white;text-align: right;padding:7px;color:white;font-family: Leelawadee;'> 
   <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>
      hello@vivocarat.com
   </a>
  </td>
 </tr>
</table>

<table style='height: 7%;width: 100%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;'>
 <tr>
  <td>
   <span style='font-size:12px;font-family: Leelawadee;'>
         Privacy Policy | Terms &amp; Conditions
   </span>  
  </td>  
 </tr>
     
 <tr>
  <td>
   <span style='font-size:12px;font-family: Leelawadee;'>
         &copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.
   </span> 
  </td>   
 </tr>
</table>

</table>
    
 </body>
</html>

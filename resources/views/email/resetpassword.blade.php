<html>
 <head>
  <style>
  
   @font-face {
            font-family: 'Leelawadee';
            src: url('https://www.vivocarat.com/fonts/Leelawadee.ttf') format('truetype');
            font-weight: normal;
            font-style: normal;
        }
        
        body {
            font-family: 'Leelawadee',sans-serif;
            font-size: 15px;
            width: 100%;
            margin-left: auto;
            margin-right: auto;
        }
 </style>
        
     <link href='https://www.vivocarat.com/fonts/Leelawadee.ttf' rel='stylesheet' type='text/css'> 
</head>

<body align='center' style="max-width:600px;text-align:center;font-family: 'Leelawadee',;font-size: 15px;margin:0 auto;background-color: #f3f3f3;">
    
<table cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>

 <tr>
  <td>
   <table cellpadding='0' cellspacing='0' align='center' style='padding-top: 10px;padding-bottom: 10px;border-collapse: collapse;'> 
    <tr>
     <td align='center'>
      <img src='https://www.vivocarat.com/images/emailers/rounded-logo.png' alt='Vivocarat logo' title='Vivocarat logo'>  
     </td>
    </tr>
   </table>
  </td>
 </tr>

 <tr>
  <table align='center' cellpadding='0' cellspacing='0' style='border-collapse: collapse;'>
   <tr>
    <td>
     <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Hi {{ $name }},
     </p>
        
     <p style='margin-left:20px;margin-right:20px;font-family: Leelawadee;'>
        Please click on the link below to reset your password
     </p>
        
     <p style='margin-left:20px;margin-right:20px;'>
      <a href="{{ $resetUrl }}" style='font-family: Leelawadee;'>
         {{ $resetUrl }}
      </a>
     </p> 
        
    </td>  
   </tr>  
  </table>  
 </tr>

<table align='center' style='background-color:#E62739;width: 100%;'>
 <tr>
  <td style='font-size:13px;color:white;text-align: left;padding:7px;color:white;font-family: Leelawadee;'>+91 9167 645 314</td>
     
  <td style='font-size:13px;color:white;text-align: right;padding:7px;color:white;font-family: Leelawadee;'>
   <a href='mailto:hello@vivocarat.com' style='text-decoration:none;color:white;'>
      hello@vivocarat.com
   </a>
  </td>
 </tr>
</table>


 <table style='height: 7%;width: 100%;padding-top: 20px;padding-bottom: 20px;text-align: center;border: 1px solid #d4d4d4;font-weight: 500;'>
 <tr>
  <td>
   <span style='font-size:12px;font-family: Leelawadee;'>
         Privacy Policy | Terms &amp; Conditions
   </span>  
  </td>  
 </tr>
     
 <tr>
  <td>
   <span style='font-size:12px;font-family: Leelawadee;'>
         &copy;&nbsp;2016 VivoCarat Retail Pvt. Ltd.All Rights Reserved.
   </span> 
  </td>   
 </tr>
</table>
    
 </table>
    
</body>

</html>
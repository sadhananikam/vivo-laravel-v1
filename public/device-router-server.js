var isServer = true;
var path = "/";

var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

/*********For Local*************/
if (isServer) {
    path = "/";

} else {
    path = "vivo-web-2";
}
var w = window.innerWidth;

//***************** Product page **********
if (w <= 1024 && ( document.location.pathname == "/p-product.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/p/"+getUrlParameter('id');}
}

//**************** Product list page *********
if (w <= 1024 && ( document.location.pathname == "/p-list.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');}
}

//***************** Individual brand/store page **********
if (w <= 1024 && ( document.location.pathname == "/p-store.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/"+getUrlParameter('store');}
}

//***************** brand/store with type page **********
if (w <= 1024 && ( document.location.pathname == "/p-splist.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');}
}

//***************** Search page **********
if (w <= 1024 && ( document.location.pathname == "/p-search.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/search/"+getUrlParameter('text');}
}

//***************** my account page **********
if (w <= 1024 && ( document.location.pathname == "/p-account.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/account";}
}

//***************** wishlist page **********
if (w <= 1024 && ( document.location.pathname == "/p-wishlist.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/wishlist";}
}

//***************** login page **********
if (w <= 1024 && ( document.location.pathname == "p-login.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/login/"+getUrlParameter('id');}
}

//***************** chekout page **********
if (w <= 1024 && ( document.location.pathname == "/p-checkout.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/checkout";}
}

//***************** home page or vivocarat.com/index.html **********
if (w <= 1024 && ( document.location.pathname == "/index.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}

//***************** orders page **********
if (w <= 1024 && ( document.location.pathname == "/p-orders.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/orders";}
} 

//***************** lookbook page **********
if (w <= 1024 && ( document.location.pathname == "/p-lookbook.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbook";}
}

//***************** Jewellery Education page **********
if (w <= 1024 && ( document.location.pathname == "/p-jewelleryeducation.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryeducation";}
} 

//***************** About page **********
if (w <= 1024 && ( document.location.pathname == "/p-about.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/about";}
} 

//***************** contact us page **********
if (w <= 1024 && ( document.location.pathname == "/p-contact.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/contact";}
} 

//***************** terms page **********
if (w <= 1024 && ( document.location.pathname == "/p-terms.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/terms";}
} 

//***************** privacy policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-privacypolicy.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/privacypolicy";}
}

//***************** return policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-returns.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/return-exchange";}
}

//***************** partner page **********
if (w <= 1024 && ( document.location.pathname == "/p-partner.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/partner";}
}

//***************** without index.hmtl **********
if (w <= 1024 && ( document.location.pathname == "")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}

//***************** brands page **********
if (w <= 1024 && ( document.location.pathname == "/p-brands.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/brands";}
}


$(window).resize(function () {
    var w = window.innerWidth;
    
//***************** Product page **********
if (w <= 1024 && ( document.location.pathname == "/p-product.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/p/"+getUrlParameter('id');}
}

//**************** Product list page *********
if (w <= 1024 && ( document.location.pathname == "/p-list.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/"+getUrlParameter('type')+"/"+getUrlParameter('subtype');}
}
    
//***************** Individual brand/store page **********
if (w <= 1024 && ( document.location.pathname == "/p-store.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/"+getUrlParameter('store');}
}    
    
//***************** brand/store with type page **********
if (w <= 1024 && ( document.location.pathname == "/p-splist.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/c/store/"+getUrlParameter('store')+"/"+getUrlParameter('type');}
}  
    
//***************** Search page **********
if (w <= 1024 && ( document.location.pathname == "/p-search.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/search/"+getUrlParameter('text');}
}
    
//***************** my account page **********
if (w <= 1024 && ( document.location.pathname == "/p-account.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/account";}
} 

//***************** wishlist page **********
if (w <= 1024 && ( document.location.pathname == "/p-wishlist.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/wishlist";}
}
    
//***************** login page **********
if (w <= 1024 && ( document.location.pathname == "/p-login.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/login/"+getUrlParameter('id');}
}
    
//***************** chekout page **********
if (w <= 1024 && ( document.location.pathname == "/p-checkout.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/checkout";}
}    

//***************** home page or vivocarat.com/index.html **********
if (w <= 1024 && ( document.location.pathname == "/index.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}  
    
//***************** orders page **********
if (w <= 1024 && ( document.location.pathname == "/p-orders.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/orders";}
}   
    
//***************** lookbook page **********
if (w <= 1024 && ( document.location.pathname == "/p-lookbook.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/lookbook";}
}
    
//***************** Jewellery Education page **********
if (w <= 1024 && ( document.location.pathname == "/p-jewelleryeducation.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/jewelleryeducation";}
}    

//***************** About page **********
if (w <= 1024 && ( document.location.pathname == "/p-about.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/about";}
}  
    
//***************** contact us page **********
if (w <= 1024 && ( document.location.pathname == "/p-contact.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/contact";}
}     

//***************** terms page **********
if (w <= 1024 && ( document.location.pathname == "/p-terms.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/terms";}
} 

//***************** privacy policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-privacypolicy.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/privacypolicy";}
}    

//***************** return policy page **********
if (w <= 1024 && ( document.location.pathname == "/p-returns.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/return-exchange";}
}
    
//***************** partner page **********
if (w <= 1024 && ( document.location.pathname == "/p-partner.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/partner";}
}
    
//***************** without index.hmtl **********
if (w <= 1024 && ( document.location.pathname == "")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/home";}
}  
    
//***************** brands page **********
if (w <= 1024 && ( document.location.pathname == "/p-brands.html")) {    
    if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/brands";}
}    

});
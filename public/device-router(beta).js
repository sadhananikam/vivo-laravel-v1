var isServer = false;
var path = "/";
var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName, i;
    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};
if (isServer) {
    path = "/";
} else {
    path = "/vivo-laravel-v1";
}
//console.log(document.location.pathname);
var w = window.innerWidth;
if (w <= 1024 && document.location.pathname == path+"/p-product.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/p/" + getUrlParameter('id');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-list.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/c/" + getUrlParameter('type') + "/" + getUrlParameter('subtype');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-store.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/" + getUrlParameter('store');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-splist.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/c/store/" + getUrlParameter('store') + "/" + getUrlParameter('type');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-search.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/search/" + getUrlParameter('text');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-account.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/account";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-wishlist.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/wishlist";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-login.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/login/" + getUrlParameter('id');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-checkout.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/checkout";
    }
}
if (w <= 1024 && document.location.pathname == path+"/index_1.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index_1.html#/i/home";
    }
}
if (w <= 1024 && (document.location.pathname == path+"/index.html")) {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/home";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-orders.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/orders";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-lookbook.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/lookbook";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-lookbookArticle.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/lookbookArticle/" + getUrlParameter('id');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-jewelleryeducation.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/jewelleryeducation";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-about.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/about";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-contact.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/contact";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-terms.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/terms";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-privacypolicy.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/privacypolicy";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-returns.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/returns";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-partner.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/partner";
    }
}
if (w <= 1024 && (document.location.pathname == path+"/")) {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/home";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-brands.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/brands";
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-resetpassword.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/resetpassword/" + getUrlParameter('token');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-paymentsuccess.html") {
    if (document.location.href.indexOf("#") === -1) {
        document.location = "m-index.html#/i/paymentsuccess/" + getUrlParameter('id');
    }
}
if (w <= 1024 && document.location.pathname == path+"/p-faq.html") { 
    if (window.location.search.indexOf('id') > -1) {
        var id = getUrlParameter('id');
        if(id.length)
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq/"+getUrlParameter('id');}
        }
        else
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
        }
    }
    else
    {
        if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
    }    
    
}
$(window).resize(function () {
    var w = window.innerWidth;
    if (w <= 1024 && document.location.pathname == path+"/p-product.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/p/" + getUrlParameter('id');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-list.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/c/" + getUrlParameter('type') + "/" + getUrlParameter('subtype');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-store.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/" + getUrlParameter('store');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-splist.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/c/store/" + getUrlParameter('store') + "/" + getUrlParameter('type');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-search.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/search/" + getUrlParameter('text');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-account.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/account";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-wishlist.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/wishlist";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-login.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/login/" + getUrlParameter('id');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-checkout.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/checkout";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/index.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/home";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-orders.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/orders";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-lookbook.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/lookbook";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-lookbookArticle.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/lookbookArticle/" + getUrlParameter('id');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-jewelleryeducation.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/jewelleryeducation";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-about.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/about";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-contact.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/contact";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-terms.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/terms";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-privacypolicy.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/privacypolicy";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-returns.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/returns";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-partner.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/partner";
        }
    }
    if (w <= 1024 && (document.location.pathname == path+"/")) {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/home";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-brands.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/brands";
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-resetpassword.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/resetpassword/" + getUrlParameter('token');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-paymentsuccess.html") {
        if (document.location.href.indexOf("#") === -1) {
            document.location = "m-index.html#/i/paymentsuccess/" + getUrlParameter('id');
        }
    }
    if (w <= 1024 && document.location.pathname == path+"/p-faq.html") {    
    if (window.location.search.indexOf('id') > -1) {
        var id = getUrlParameter('id');
        if(id.length)
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq/"+getUrlParameter('id');}
        }
        else
        {
            if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
        }
    }
    else
    {
        if(document.location.href.indexOf("#") === -1){ document.location = "m-index.html#/i/faq";}
    } 
} 
});
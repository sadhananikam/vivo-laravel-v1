var productapp = angular.module("vivoTestProduct", ['vivoCommon','angularFileUpload']);

productapp.directive('ngThumb', ['$window', function($window) {
        var helper = {
            support: !!($window.FileReader && $window.CanvasRenderingContext2D),
            isFile: function(item) {
                return angular.isObject(item) && item instanceof $window.File;
            },
            isImage: function(file) {
                var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
                return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
            }
        };

        return {
            restrict: 'A',
            template: '<canvas/>',
            link: function(scope, element, attributes) {
                if (!helper.support) return;

                var params = scope.$eval(attributes.ngThumb);

                if (!helper.isFile(params.file)) return;
                if (!helper.isImage(params.file)) return;

                var canvas = element.find('canvas');
                var reader = new FileReader();

                reader.onload = onLoadFile;
                reader.readAsDataURL(params.file);

                function onLoadFile(event) {
                    var img = new Image();
                    img.onload = onLoadImage;
                    img.src = event.target.result;
                }

                function onLoadImage() {
                    var width = params.width || this.width / this.height * params.height;
                    var height = params.height || this.height / this.width * params.width;
                    canvas.attr({ width: width, height: height });
                    canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
                }
            }
        };
    }]);

productapp.controller('productCtrl',['$scope','$rootScope','$location','$http','Data','$window','$timeout','FileUploader','API_URL', function ($scope, $rootScope, $location, $http, Data, $window,$timeout,FileUploader,API_URL) {
  $scope.imagename = [];
  $scope.resetcustomize = function () {
          $scope.customizef = {};
  }
  $scope.resetcustomize();

  $scope.saveCustomizeform = function(){
    $scope.imagename = [];
    //console.log($scope.uploader.queue.length);
      waitingDialog.show();
      if($scope.uploader.queue.length > 0)
      {
        $scope.uploader.uploadAll();
      }
      else
      {
        $scope.saveCustomizeformData();
      }
      //console.log($.param($scope.customizef));
      //console.log($scope.uploader.queue);
  };

  $scope.saveCustomizeformData = function(){
      var url = API_URL + 'saveCustomizeformData';
      $http({
          method: 'POST',
          url: url,
          params : {
              name: $scope.customizef.cmname,
              email: $scope.customizef.cmemail,
              phone: $scope.customizef.cmphone,
              comment: $scope.customizef.cmcomment,
              imagename: JSON.stringify($scope.imagename)
          }
      }).then(function successCallback(response){
          console.log(response.data);
          waitingDialog.hide();
          if(response.data === 'success')
          {
              $scope.imagename = [];
              $scope.uploader.clearQueue();
              $scope.resetcustomize();
              $("#CustomizeRing").modal('hide');

              $("#customizeSuccess").modal('show');
              $timeout(function () {
                  $("#customizeSuccess").modal('hide');
              }, 2000);
          }
          else if(response.data === 'error')
          {
              $scope.imagename = [];
              $scope.uploader.clearQueue();
              $scope.resetcustomize();
              $("#CustomizeRing").modal('hide');

              $("#customizeError").modal('show');
              $timeout(function () {
                  $("#customizeError").modal('hide');
              }, 2000);
          }
      },function errorCallback(response){
          console.log(response.data);
          waitingDialog.hide();
          $scope.imagename = [];
          $scope.uploader.clearQueue();
          $scope.resetcustomize();
          $("#CustomizeRing").modal('hide');

          $("#customizeError").modal('show');
          $timeout(function () {
              $("#customizeError").modal('hide');
          }, 2000);
      });
  };

    // start of file upload code
    var uploader = $scope.uploader = new FileUploader({
        url: API_URL + 'uploadimages'
    });

    // FILTERS

    uploader.filters.push({
        name: 'imageFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
            return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
        }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
        //console.log(response.filename);
        $scope.imagename.push(response.filename);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
        console.log($scope.imagename);
        $scope.saveCustomizeformData();
    };

    console.info('uploader', uploader);

    // end of file upload code

    $scope.id = getUrlParameter('id');

    $scope.viewMore = function () {
        $("html,body").animate({
            scrollTop: $('#product-details').position().top
        }, 800);
    }

    $scope.review = {
        pid: null,
        uid: null,
        name: null,
        quality: null,
        price: null,
        value: null,
        review: null
    }

    $scope.addReview = function () {

        if ($rootScope.authenticated) {
            $scope.review.pid = $scope.id;
            $scope.review.uid = $rootScope.uid;
            $scope.review.name = $rootScope.name;

            if ($scope.review.price == null || $scope.review.price == null || $scope.review.value == null || $scope.review.value == null || $scope.review.quality == null || $scope.review.quality == "" || $scope.review.review == null || $scope.review.review == "") {

                $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
            }
            else
            {
                $http({
                    method: 'POST',
                    url : API_URL + 'addReview',
                    params : {
                    review:JSON.stringify($scope.review)
                        }
                }).then(function successCallback(response){
                    console.log(response.data);
                    $scope.getReviews();
                    //Review added successfully
                    $("#reviewSuccess").modal('show');

                    $timeout(function () {
                        $("#reviewSuccess").modal('hide');
                    }, 2000);
                },function errorCallback(response){
                    console.log(response.data);
                    //Review error
                    $("#reviewError").modal('show');

                    $timeout(function () {
                        $("#reviewError").modal('hide');
                    }, 2000);
                });
            }

        } else {
            //Please login to add review.
            $("#login").modal('show');

            $timeout(function () {
                $("#login").modal('hide');
            }, 2000);
        }
    }

    $scope.getCumulativeReview = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getCumulativeReview',
            params : {pid:$scope.id}
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.cummReview = response.data[0];
            $scope.cummReview.v == null ? $scope.v = 0 : $scope.v = parseFloat($scope.cummReview.v);
            $scope.cummReview.q == null ? $scope.q = 0 : $scope.q = parseFloat($scope.cummReview.q);
            $scope.cummReview.p == null ? $scope.price = 0 : $scope.price = parseFloat($scope.cummReview.p);
            $scope.totalRating = ($scope.q + $scope.price + $scope.v) / 3;
        },function errorCallback(response){
            console.log(response.data);
            //Cumulative Review error
            $("#reviewCummErr").modal('show');

            $timeout(function () {
                $("#reviewCummErr").modal('hide');
            }, 2000);
        });

    }

    $scope.getReviews = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getReviewList',
            params : {pid:$scope.id}
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.reviewList = response.data;
            $scope.getCumulativeReview();
        },function errorCallback(response){
            console.log(response.data);
            //Review List error
            $("#reviewListErr").modal('show');

            $timeout(function () {
                $("#reviewListErr").modal('hide');
            }, 2000);
        });
    }
    $scope.getReviews();

    $scope.customize = function (p) {
        $("#CustomizeRing").modal('show');
    }

    $scope.buyNow = function (p) {

        if (p.category == 'Bracelets') {
            if (p.bracelet_size == null) {

                //Please select a braclet size
                $("#BraceletSize").modal('show');

                $timeout(function () {
                    $("#BraceletSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
//                    params : {
//                            items: JSON.stringify($rootScope.cart),
//                            uid : $rootScope.uid
//                        }
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    console.log(response.data);
                    window.location.href = "p-checkout.html";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.html";
            }
            }

        } else if (p.category == 'Rings') {
            if (p.ring_size == null) {

                //Please select a ring size
                $("#RingSize").modal('show');

                $timeout(function () {
                    $("#RingSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
//                    params : {
//                            items: JSON.stringify($rootScope.cart),
//                            uid : $rootScope.uid
//                        }
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    console.log(response.data);
                    window.location.href = "p-checkout.html";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.html";
            }
            }

        } else if (p.category == 'Bangles') {
            if (p.bangle_size == null) {

                //Please select a bangle size
                $("#BangleSize").modal('show');

                $timeout(function () {
                    $("#BangleSize").modal('hide');
                }, 2000);
            } else {

                $rootScope.addToCart(p);
                if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
//                    params : {
//                            items: JSON.stringify($rootScope.cart),
//                            uid : $rootScope.uid
//                        }
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    console.log(response.data);
                    window.location.href = "p-checkout.html";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.html";
            }
        }

        } else {
            $rootScope.addToCart(p);
            if($rootScope.uid !== undefined && $rootScope.uid !== null){
                $http({
                    method: 'POST',
                    url : API_URL + 'savetocart',
//                    params : {
//                            items: JSON.stringify($rootScope.cart),
//                            uid : $rootScope.uid
//                        }
                    data: {
                        items: JSON.stringify($rootScope.cart),
                        uid: $rootScope.uid
                    }
                }).then(function successCallback(response){
                    console.log(response.data);
                    window.location.href = "p-checkout.html";
                },function errorCallback(response){
                    console.log(response.data);
                });
            }
            else{
                window.location.href = "p-checkout.html";
            }
        }

    }

    $scope.ratingQuantity = 5;
    $scope.ratingPrice = 3;
    $scope.ratingValue = 2;
    $scope.rateFunction = function (rating) {
        alert('Rating selected - ' + rating);
    };

    $scope.sendAlert = function (email, phone, size) {

        $http({
            method: 'GET',
            url : API_URL + 'setAlert',
            params : {
                    phone:phone,
                    id:$scope.p.id,
                    email:email,
                    size:size
                }
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.isAlert = false;
        },function errorCallback(response){
            console.log(response.data);
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });

    }

    $scope.isOutOfStock = false;

    $scope.getProductVariant = function (v, type) {

        $http({
            method: 'GET',
            url : API_URL + 'getProductVariant',
            params : {
                    sku:$scope.p.VC_SKU,
                    v:v,
                    type:type
                }
        }).then(function successCallback(response){
            console.log(response.data);
            if (response.data == "") {
                $scope.isOutOfStock = true;
            } else {
                $scope.p.id = response.data[0].id;
                $scope.isOutOfStock = false;
                $scope.p.VC_SKU = response.data[0].VC_SKU;
                $scope.p.VC_SKU_2 = response.data[0].VC_SKU_2;
                $scope.p.price_after_discount = response.data[0].price_after_discount;
                $scope.p.price_before_discount = response.data[0].price_before_discount;
                $scope.deliveryDate = $rootScope.date.AddDays($scope.p.estimated_delivery_time);
                $scope.img_count = parseInt(response.data[0].img_count);
            }
        },function errorCallback(response){
            console.log(response.data);
            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }

    $scope.productId = $scope.id;

    $('a[data-toggle="collapse"]').on('click', function () {

        var objectID = $(this).attr('href');

        if ($(objectID).hasClass('in')) {
            $(objectID).collapse('hide');
        } else {
            $(objectID).collapse('show');
        }
    });


    $scope.getProduct = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getProductDetail',
            params : {id:$scope.productId}
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.p = response.data[0];
            $scope.p.ring_size = null;
            $scope.p.bangle_size = null;
            $scope.p.bracelet_size = null;
            $scope.getSimilarProducts();
            $scope.getSimilarJewellerProducts();
            $scope.getJewellerInformation();
            $scope.getProductSize();
            $scope.deliveryDate = $rootScope.date.AddDays($scope.p.estimated_delivery_time);
            $scope.img_count = parseInt(response.data[0].img_count);
        },function errorCallback(response){
            console.log(response.data);

            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }

    $scope.getProductSize = function (){
        var category = $scope.p.category;
        var sizetype = '';
        if(category === 'Rings'){
            sizetype = 'ring_size';
        }else if(category === 'Bangles'){
            sizetype = 'bangle_size';
        }else if(category === 'Bracelets'){
            sizetype = 'bracelet_size';
        }

        if(sizetype != null && sizetype != ''){
            $http({
            method: 'GET',
            url : API_URL + 'getProductSize',
            params : {
                    sku:$scope.p.VC_SKU,
                    category:category,
                    sizetype:sizetype
                }
            }).then(function successCallback(response){
                console.log(response.data);
                $scope.psize = response.data;
            },function errorCallback(response){
                console.log(response.data);
            });
        }
    }

    $scope.getJewellerInformation = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getJewellerInformation',
            params : {
                    s:$scope.p.jeweller_id
                }
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.jeweller = response.data[0];
        },function errorCallback(response){
            console.log(response.data);

            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }

    $scope.getProduct();
    $scope.getSimilarJewellerProducts = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getSimilarJewellerProducts',
            params : {s:$scope.p.supplier_name}
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.list = response.data;
        },function errorCallback(response){
            console.log(response.data);

            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);
        });
    }


    $scope.getSimilarProducts = function () {

        $http({
            method: 'GET',
            url : API_URL + 'getSimilarProductsByCat',
            params : {c:$scope.p.category}
        }).then(function successCallback(response){
            console.log(response.data);
            $scope.same_list = response.data;

            $('#etalage').etalage({
                thumb_image_width: 360,
                thumb_image_height: 360,
                source_image_width: 900,
                source_image_height: 900,
                show_hint: true,
                click_callback: function (image_anchor, instance_id) {
                },
                zoom_area_width: 360,
                zoom_area_height: 360,
                source_image_width: 700,
                source_image_height: 700
            });

        },function errorCallback(response){
            console.log(response.data);

            $("#jewelerr").modal('show');

            $timeout(function () {
                $("#jewelerr").modal('hide');
            }, 2000);

        });
    }

}]);

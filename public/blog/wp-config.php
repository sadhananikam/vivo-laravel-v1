<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'vivoc7vf_wp35');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e07uains2fhw7aojlump2pxlldaxgmebxocvrpbjl15980ndc7epbfsqpjnln1io');
define('SECURE_AUTH_KEY',  'lpmj54xe2luzrmynamuyu45osvruutidnxv3hzehe1tfnkzuwjlr0qt3hc5fnuuh');
define('LOGGED_IN_KEY',    'e4ibq13dmy6mrlzttj3uezmbpqombsahpez2vdcsxlpvi5cr8pwjsgqz21dnlnmt');
define('NONCE_KEY',        'zhrrlzwqgmavuprvgikrqckrkx96o3vs5xonkckgxenkiroetqxbofdkgvxohxya');
define('AUTH_SALT',        'qgdbowsbfghati1nehnonxvxugj8smv1owgj1j0o5rdjk6puybhqmccjggs9hpfo');
define('SECURE_AUTH_SALT', '052gzjogs0b7nlvrwrmk5ny7apl9ctmecvuikeklun3o0circh9wuvgknjuyfdig');
define('LOGGED_IN_SALT',   'pe4whbhbdskcrk6d20gc31xklgtcz5gkuwopemkaxhzq3miejei0mh11lnytvoup');
define('NONCE_SALT',       '6kuquycwfhyemfhkealwbz03vpyafvsnstclkkregpcrglnqtpjwnyrxiro7chws');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wpwg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

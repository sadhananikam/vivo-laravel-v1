<?php
//echo $_SERVER['SERVER_NAME'];
//$ogtitle = "Dynamic title test";
$tag = $_GET['text'];
$SITE_ROOT = "https://www.vivocarat.com/";

$jsonData = getData($SITE_ROOT,$tag);
$jsonData = json_decode($jsonData);

$title = $jsonData->page_title;
$meta_keywords = $jsonData->meta_keywords;
$meta_description = $jsonData->meta_description;
$meta_robots = $jsonData->meta_robots;
$og_type = $jsonData->og_type;
$og_title = $jsonData->og_title;
$og_image = $jsonData->og_image;
$og_description = $jsonData->og_description;

$description = $jsonData->description;
    
function getData($siteRoot,$tag) {

    $rawData = file_get_contents($siteRoot.'api/v1/gettagdescription?tag='.$tag);
    return $rawData;
}
?>
<!DOCTYPE html>
<html lang="en" data-ng-controller='searchCtrl' data-ng-app="vivoSearch">

<head> 
    <title><?php echo $title; ?></title>
    <meta name="keywords" content="<?php echo $meta_keywords; ?>" >
    <meta name="description" content="<?php echo $meta_description; ?>" />
    <meta name="robots" content="<?php echo $meta_robots; ?>" >
    <meta property="og:description" content="<?php echo $og_description; ?>" />
    <meta property="og:image" content="<?php echo $og_image; ?>" />
    <meta property="og:title"         content="<?php echo $og_title; ?>" />
    <meta property="og:type"          content="<?php echo $og_type; ?>" />
    
    <meta http-equiv="Content-Language" content="en" />

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="author" content="Vivo">
    <link rel="icon" href="images/icons/favico.png" type="image/x-icon" />

<!--    <link rel="canonical" href="http://www.vivocarat.com/" />-->

    <!-- SEO-->
    <meta name="google-site-verification" content="d29imIOMXVw4oDrvX0W26H7Dg3_nAHDi75mhXZ5Wpc4" />

    <link rel="stylesheet" href="css/kendo.common-material.min.css" />
    <link rel="stylesheet" href="css/kendo.material.min.css" />


    <noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1026045347487468&ev=PageView&noscript=1" /></noscript>
    <!-- End Facebook Pixel Code -->

    <link rel="stylesheet" href="css/arthref.css">

    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/form.css" rel="stylesheet" type="text/css" media="all" />

    <!--end slider -->
    <!-- start menu -->
    <link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/magnific-popup.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/etalage.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/megamenu.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/angular.rangeSlider.css" rel="stylesheet" type="text/css" media="all" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.css">
    <link rel="stylesheet" type="text/css" href="css/daterangepicker.css" />

    <link href="css/bootstrap.min.css" rel="stylesheet">
    
        <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
    n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
    document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '293278664418362', {
    em: 'insert_email_variable,'
    });
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=293278664418362&ev=PageView&noscript=1"
    /></noscript>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->
</head>

<body style="overflow-x: hidden;" ng-cloak>

<style>
    .no-padding{
        padding:0px;
    }
    .no-pad {
        padding: 0px;
    }


    .ngrs-range-slider .ngrs-join {
        position: absolute;
        z-index: 1;
        top: 50%;
        left: 0;
        right: 100%;
        height: 8px;
        margin: -4px 0 0 0;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        border-radius: 4px;
        background-color: #E22B46;
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
        background-size: 100%;
        background-image: -webkit-gradient(linear, 50% 0%, 50% 100%, color-stop(0%, #E22B46), color-stop(100%, #E22B46));
        background-image: -moz-linear-gradient(#5bc0de, #2f96b4);
        background-image: -webkit-linear-gradient(#5bc0de, #2f96b4);
        background-image: linear-gradient(#E22B46, #E22B46);
    }

    .vivo-grid {
        padding: 3px;
        width: 100%;
        height: 334px;
    }

    .vivo-box-padding {
        padding: 2px;
    }
    
    /*Done to adjust dropdown of jewellers within the filter box area*/
        .k-animation-container{
            height: 117px !important;
        }
        
        .k-list-container{
            height: 117px !important;
        }
        
        .k-list-scroller{
            height: 117px !important;
        }
        
        .k-popup.k-list-container {
            padding-top: 0px !important;
        }

</style>

<vivo-header></vivo-header>

<div >

<div class="container no-padding-left-right padding-bottom-60px">

    <div class="mens">
        <div class="main margin-none">

            <div class="row">

                <div class="col-sm-12 cont no-padding-left-right">
                    <div class="row">
                        <div class="col-xs-12 padding-top-5px" style="padding-left: 30px;padding-right: 30px;">
                            <img class="img-responsive width-100-percent" ng-src="{{banImg}}" err-src="{{ defaultImage }}">
                        </div>
                    </div>

                    <div class="container" style="padding-left: 30px;padding-right: 30px;">
                        <div class="row vivo-main-header subheader padding-top-0px margin-top-0px" style="padding-bottom: 12px;">
                            <div class="col-sm-6 padding-top-10px">
                                <h class="head result-for">Results for: {{resultfor}}</h>
                            </div>

                            <div class="col-sm-6 filter-header-text padding-top-10px" style="padding-bottom:8px;">
                                    <h class="pull-right normal-text">
                                        Total {{count}} items
                                    </h>
                            </div>
<!--
                                        <a style="color:white !important; padding: 0 0px 0 10px" data-ng-click="setLayout(true)"><img style="width:20px" src="images/grid.png"></a>
                                        <a style="color:white !important; padding: 0 0px 0 5px" data-ng-click="setLayout(false)"><img style="width:20px" src="images/list.png"></a>
-->
<div class="row">
 <div class="col-sm-12">
                                <div class="row normal-text bold-font">
                                    <div class="col-sm-6 filter-header-text" style="width: 498px;"></div>
                                    <div class="col-sm-1 text-center filter-header-text pointer normal-text bold-font no-padding-left-right" style="font-size: 15px;border: solid 1px #EF8D96;" data-ng-mouseover="openFilter()">
                                        Filter by<img src="images/list/icons/Arrow.jpg" style="padding-left: 4px;">
                                    </div>

                                    <div class="col-sm-2 text-right pull-right grey-colour no-padding-right">
                                        <div class="sort-by filter-header-text">
                                            Sort By
                                            <select name="singleSelect" id="singleSelect" data-ng-model="selectedFilter" class="ng-pristine ng-valid ng-touched">
                                                <option value="">Popularity</option>
<!--                                                <option value="Name">Name</option>-->
                                                <option value="Price L to H">Price L to H </option>
                                                <option value="Price H to L">Price H to L</option>
                                            </select>

                                        </div>
                                    </div>


                                </div>
                            </div>
</div>


<div class="row">
 <div class="col-sm-12" style="display:none" id="filterArea">
                                <div class="row normal-text" style="margin-top:20px;">
                                    <div class="col-sm-3 filter-header-text normal-text no-padding-left" style="padding-right: 80px;"> Price(in Rs.)
                                        <br/>
                                        <div range-slider min="0" max="100000" model-min="demo1.min" model-max="demo1.max"></div>
                                        <div class="row">
                                            <div class="col-xs-5 no-padding-left">

                                                <input class="width-100-percent padding-left-5px" placeholder="Min" style="width:100%" type="number" ng-model="demo1.min">
                                            </div>

                                            <div class="col-xs-2 no-padding-left-right" style="margin-top: 4px;">TO</div>

                                            <div class="col-xs-5 no-padding-left-right">

                                                <input class="width-100-percent padding-left-5px" type="number" placeholder="Max" style="width:100%" ng-model="demo1.max">
                                            </div>
                                        </div>
                                    </div>
                                     <div class="col-md-2 normal-text no-padding-left">
        <p>Metal</p>

    <div class="row">
     <div class="col-md-12 no-pad">
      <label class="filter-text">
       <input class="checkbox-position" type="checkbox" data-ng-model="isGold">
          Yellow Gold
      </label>
     </div>

     <div class="col-md-12 no-padding-left-right">
      <label class="filter-text">
       <input class="checkbox-position" type="checkbox" data-ng-model="isWhiteGold">
          White gold
      </label>
     </div>

     <div class="col-md-12 no-padding-left-right">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="isRoseGold">
          Rose Gold
      </label>
     </div>

     <div class="col-md-12 no-pad">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="isSilver">
          Silver
      </label>
     </div>
    </div>

   </div>
   <div class="col-md-2 normal-text">
        <p>Metal purity</p>

    <div class="row">
     <div class="col-md-12 no-pad">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="is22">
          22 KT
      </label>
     </div>

     <div class="col-md-12 no-pad">
      <label class="filter-text">                                                  <input class="checkbox-position" type="checkbox" data-ng-model="is18">
          18 KT
      </label>
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">                                                    <input class="checkbox-position" type="checkbox" data-ng-model="is14">
         14 KT
     </label>
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">
      <input class="checkbox-position" type="checkbox" data-ng-model="isOther">
        Other
     </label>
    </div>
   </div>
  </div>

  <div class="col-md-2 normal-text">
       <p>Occasion</p>

   <div class="row">
    <div class="col-md-12 no-pad">
     <label class="filter-text">                                                    <input class="checkbox-position" type="checkbox" data-ng-model="isCasual">
         Casual
     </label>
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">                                                    <input class="checkbox-position" type="checkbox" data-ng-model="isFashion">
         Fashion
     </label>
    </div>

    <div class="col-md-12 no-pad">
     <label class="filter-text">
      <input class="checkbox-position" type="checkbox" data-ng-model="isBridal">
         Bridal
     </label>
    </div>
   </div>
  </div>
                                    <div class="col-sm-3 normal-text no-padding-right">

                                        <div class="demo-section k-content">

                                            <select kendo-multi-select k-options="selectOptions" k-ng-model="jewellers"></select>

                                        </div>

                                    </div>



                                </div>



                                <div class="row">

                                    <div class="col-sm-12 text-right no-padding-right">
                                        <a data-ng-click="resetFilter()" class="btn btn-list">
                                            Clear
                                        </a>

                                        <a data-ng-click="filter()" class="btn btn-list">
                                            Search
                                        </a>

                                        <a data-ng-click="closeFilter()">
                                            <img src="images/list/icons/close.png">
                                        </a>

                                    </div>
                                </div>
                            </div>
</div>


                        </div>

                    </div>

                    <vivo-product-list></vivo-product-list>

                    <div class="row" id='loadmore' tagged-infinite-scroll='loadMore()'></div>

                </div>
            </div>
            <div class="clear"></div>

        </div>
    </div>

</div>

<!-- start tag description  -->
        <div class="middle-footer">
          <div class="container">
            <div class="row">
              <div class="col-md-12">
                <p><?php echo $description; ?></p>
              </div>
            </div>
          </div>
        </div>
<!-- end tag description  -->    

</div>
<vivo-footer></vivo-footer>
<script src="js/jquery.js"></script>
<!--start slider -->
<link rel="stylesheet" href="css/fwslider.css">
<script src="js/jquery-ui.min.js"></script>
<script src="js/css3-mediaqueries.js"></script>

<script type="text/javascript" src="js/megamenu.js"></script>

<script src="js/slides.min.jquery.js">
</script>
<script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="js/jquery.easydropdown.js"></script>

<script src="js/bootstrap.min.js"></script>

<script src="js/custom.js"></script>

<script src="js/socialShare.js"></script>
<script src="js/angular.min.js"></script>
<script src="js/angular-ui-router.min.js"></script>
<script src="js/angular-animate.min.js"></script>
<script src="js/angular-sanitize.js"></script>
<script src="js/satellizer.min.js"></script>
<script src="js/angular.rangeSlider.js"></script>
<script src="js/select.js"></script>
<script src="js/toaster.js"></script>
<script src="js/kendo.all.min.js"></script>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script src="js/updateMeta.js"></script>
<script src="js/update-meta.directive.js"></script>
<script src="js/update-title.directive.js"></script>
<script src="js/taggedInfiniteScroll.js"></script>

<!-- Plugin JavaScript -->
<script src="js/jquery.easing.min.js"></script>
<script src="js/angular-google-plus.min.js"></script>

<script src="js/jquery.etalage.min.js"></script>
<script src="js/jquery.magnific-popup.js"></script>

<!--  start angularjs modules  -->
<script src="app/modules/vivoCommon.js"></script>
<script src="app/modules/vivoSearch.js"></script>
<!-- end angularjs modules -->

<script src="app/data.js"></script>
<script src="app/directives.js"></script>

<!-- Start include Controller for angular -->
<script src="app/ctrls/footerCtrl.js"></script>
<!--  Start include Controller for angular -->

    <script src="device-router.js"></script>


<script type="text/javascript" src="js/moment.min.js"></script>
<script type="text/javascript" src="js/daterangepicker.js"></script>

<!--Scroller script-->
<script type="text/javascript" src="js/jquery.simplyscroll.js"></script>

<script src="js/fwslider.js"></script>

<link rel="stylesheet" href="css/bootstrap-dialog.min.css">
<script type="text/javascript" src="js/bootstrap-dialog.min.js"></script>
      

<script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-67690535-1', 'auto');
 ga('send', 'pageview');

</script>

</body>
</html>
